# Neuro Website

HTML site hosted using GitLab Pages.

## Publishing Current Version

The website can be generated using Hugo and pushed to the current GitLab repository using

```
./make_neuro.sh
```

and entering a commit message.

## Placeholder Variables

In `config.toml` custom variables can be defined in the `[Params]` section. E.g. the variable `nextmeet` defines the date of the next meeting and is applied in multiple places, e.g. in the `content/_index.md` start page, by using a placeholder like this: `{{< param nextmeet >}}`. 
Compare, for example: https://discourse.gohugo.io/t/using-a-site-global-string/15554/6
## Hugo

Markdown files in content are converted to html using [Hugo](https://gohugo.io/). Hugo can be installed, e.g. on Debian, like this:

```
apt-get install hugo
```

## GitLab CI

This project's static Pages are built by GitLab CI, following the steps defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: busybox

pages:
  stage: deploy
  script:
    - echo "The site will be deployed to $CI_PAGES_URL"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

The above example expects to put all your HTML files in the `public/` directory.

## GitLab Pages

GitLab Pages are activated using the project's **Settings**. The url of the generated website is https://neuro.pages.ub.uni-bielefeld.de/neuro
