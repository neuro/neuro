---
title: "Vorträge und Tutorials"
subtitle: ""
tags: [resources]
---

### Zum Einstieg

#### Vorlesung Kognitive Neuro

{{< rawhtml >}}    
    <iframe src="https://uni-bielefeld.cloud.panopto.eu/Panopto/Pages/Embed.aspx?id=b2b8ad37-abdd-4ed3-8ae6-af8f00b505a2&autoplay=false&offerviewer=true&showtitle=true&showbrand=true&captions=false&interactivity=all" height="405" width="720" style="border: 1px solid #464646;" allowfullscreen allow="autoplay"></iframe>
{{< /rawhtml >}}

#### Tutorials und Bücher:

* Wie können wir Gedanken lesen? https://ldeitermann.pages.ub.uni-bielefeld.de/fmrt-explained/intro.html
* Neuroimaging Data Science https://neuroimaging-data-science.org/root.html
* Neurohackademy https://neurohackademy.org/
* Aperture Neuro https://apertureneuro.org/
* Handbook of fMRI data analysis (Poldrack, Mumford, Nichols): https://katalogplus.ub.uni-bielefeld.de/Record/991025810618306442 
* VBM Tutorial, Ashburner: https://www.fil.ion.ucl.ac.uk/~john/misc/VBMclass15.pdf
* SPM Course 2011: https://www.fil.ion.ucl.ac.uk/spm/course/video/

