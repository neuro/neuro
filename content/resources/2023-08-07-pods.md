---
title: "Podcasts"
subtitle: ""
tags: [resources]
---

Hier sammeln wir ein Liste von Podcasts, in denen fMRT Forschung und Datenanalyse diskutiert werden

* Brain Inspired | https://braininspired.co/podcast
* The Language Neuroscience Podcast | https://langneurosci.org/podcast
* Talk Python to Me | https://talkpython.fm/episodes/all
