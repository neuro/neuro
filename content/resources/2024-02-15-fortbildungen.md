---
title: "Fortbildungsmöglichkeiten"
subtitle: ""
tags: [resources]
---

#### Fortbildungen, Workshops, etc.

* Epileptologisches Kolloquium Mara (Mo, 16:30): https://www.mara.de/universitaetsklinik-fuer-epileptologie/fortbildung/epileptologisches-und-neurowissenschaftliches-kolloquium 
* Neurozentrum Bethel (Do, 17:00): https://www.mara.de/universitaetsklinik-fuer-epileptologie/fortbildung/neurozentrum-bethel
* ILAE Neuroimaging School: https://www.ilae.org/congresses
* Neurohackademy: https://neurohackademy.org/
* Neuromatch: https://academy.neuromatch.io/

