---
title: "Foren"
subtitle: ""
tags: [resources]
---

Hier sammeln wir ein Liste von Foren, in denen fMRT-Forschung und Datenanalyse diskutiert werden

* Neurostars | https://neurostars.org/
* SPM Mailing List | https://www.jiscmail.ac.uk/cgi-bin/webadmin?A0=SPM
* freesurfer Mailing List | https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/

