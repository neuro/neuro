---
title: "3. Treffen"
subtitle: "16:15 vor Ort"
date: 2023-12-10
tags: [termine]
---

Im dritten Treffen wurden folgende Themen diskutiert:

Wie prozessiere ich resting-state fMRT-Daten?
* Drifts
* Signal in weißer Substanz und CSF
* Denoising
* PCA
* Bewegungsparameter und frame-wise displacement
* High-Pass und Low-Pass Filter
* AROMA-ICA 

Relevante Artikel:
* A method for using blocked and event-related fMRI data to study “resting state” functional connectivity https://doi.org/10.1016/j.neuroimage.2006.11.051
* Task-free MRI predicts individual differences in brain activity during task performance https://doi.org/10.1126/science.aad8127
* Reconfigurable task-dependent functional coupling modes cluster around a core functional architecture https://royalsocietypublishing.org/doi/abs/10.1098/rstb.2013.0526

Danke an alle, die teilgenommen haben!
