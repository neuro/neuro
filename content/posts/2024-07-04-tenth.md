---
title: "10. Treffen"
subtitle: "16:15 vor Ort"
date: 2024-07-04
tags: [termine]
---

Im zehnten Treffen wurden folgende Themen diskutiert:

* Wie sieht eine SPM Onset Datei aus (für First Level -> Multiple Conditions)? ([Vorlage](/myOnsets.m))
* Wie können wir aus einer Psychopy .log Datei eine Matlab .m Datei für SPM machen? ([Python-Skript](/make-m.ipynb))

Danke an alle, die teilgenommen haben!
