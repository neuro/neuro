---
title: "12. Treffen - 2nd Level Designs"
subtitle: "16:15 vor Ort"
date: 2024-10-10
tags: [termine]
---

Im zwöften Treffen wurden folgende Themen diskutiert:

* Was sind beta-dateien und con-datein in SPM? Wie werden Sie für Gruppenanlysen genutzt
* Wie sieht ein einfaches Second-Level-Design aus? ([fil.ion.ucl.ac.uk/spm/data/face_rfx/](https://www.fil.ion.ucl.ac.uk/spm/data/face_rfx/))

Danke an alle, die teilgenommen haben!
