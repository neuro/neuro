---
title: "2. Treffen"
subtitle: "16:15 vor Ort"
date: 2023-11-02
tags: [termine]
---

Im zweiten Treffen wurden folgende Themen diskutiert:

Wie erstelle ich ein event-related Experiment?
* Länge der Abstände zwischen Trials
* Variabilität von Jittern
* Abtasten der HRF durch Onsets die nicht durch die TR teilbar sind
* Parametrische Designs oder nur Extreme Abstufungen?
* Wie mit Motorikartefakten beim Drücken von Tasten umgehen?
* evtl. relevant: https://doi.org/10.1371%2Fjournal.pone.0126255

Danke an alle, die teilgenommen haben!
