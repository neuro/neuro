![LOGO](fig.jpg "neuro")

## Willkommen!

auf der Website des fMRT-Kolloquiums der Uni Bielefeld

## Treffen

Das nächste Treffen ist am Donnerstag, {{< param nextmeet >}} von 16:15 bis 17:45.
| Datum    | Ort    |      Thema      |
|----------|--------|-----------------|
| {{< param nextmeet >}}, 16:15  | U2-232 | ANOVAs |

Wir freuen uns auf Eure Teilnahme!  

## Kontakt

* [martin.wegrzyn@uni-bielefeld.de](mailto:martin.wegrzyn@uni-bielefeld.de)
