% regular version

names = cell (1,4);
names{1} = 'language';
names{2} = 'spatial';
names{3} = 'face';
names{4} = 'motor';

onsets = cell (1,4);
onsets{1} = [ 10 50 110 170 200];
onsets{2} = [ 20 70 100 160 210];
onsets{3} = [ 30 60 120 150 230];
onsets{4} = [ 40 90 140 190 240];

durations = cell (1,4);
durations{1} = 10;
durations{2} = 10;
durations{3} = 10;
durations{4} = 10;

save ('myOns1.mat', 'names','onsets', 'durations')



% param version

names = cell (1,1);
names{1} = 'all';

onsets = cell (1,1);
onsets{1} = [ 10 20 30 40 50 60 70 90 100 110 120 140 150 160 170 190 200 210 230 240];

durations = cell (1,1);
durations{1} = 10;


pmod = struct('name',{''},'param',{},'poly',{});

pmod(1).name{1} = 'language';
pmod(1).param{1} = [1 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 1 0 0 0];
pmod(1).poly{1} = 1;

pmod(1).name{2} = 'spatial';
pmod(1).param{2} = [0 1 0 0 0 0 1 0 1 0 0 0 0 1 0 0 0 1 0 0];
pmod(1).poly{2} = 1;

pmod(1).name{3} = 'face';
pmod(1).param{3} = [0 0 1 0 0 1 0 0 0 0 1 0 1 0 0 0 0 0 1 0];
pmod(1).poly{3} = 1;

pmod(1).name{4} = 'motor';
pmod(1).param{4} = [0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1 0 0 0 1];
pmod(1).poly{4} = 1;

orth = {0};

save ('myParamOns1.mat', 'names','onsets', 'durations','pmod','orth')